<?php
/**
 * Description: Lead Model. This is the model to the lead.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */

class Lead_model extends CI_Model
{
    public $lead_id = null;
    public $lead_name;
    public $lead_email;
    public $lead_phone;
    public $lead_country;
    public $lead_campaign;
    public $lead_source;
    public $lead_medium;

    public function __construct()
    {
        $this->load->database();
    }

    public function load($array = null)
    {
        if (!empty($array)) {
            $this->lead_id = isset($array['lead_id']) ? $array['lead_id'] : null;
            $this->lead_name = isset($array['name']) ? $array['name'] : '';
            $this->lead_email = isset($array['email']) ? $array['email'] : '';
            $this->lead_phone = isset($array['phone']) ? $array['phone'] : '';
            $this->lead_country = isset($array['country']) ? $array['country'] : '';
            $this->lead_campaign = isset($array['campaign']) ? $array['campaign'] : '';
            $this->lead_source = isset($array['source']) ? $array['source'] : '';
            $this->lead_medium = isset($array['medium']) ? $array['medium'] : '';
            return true;
        } else {
            return false;
        }
    }

    public function get_leads()
    {
        return $this->db->select('t1.*, t2.country_name')
            ->from('tbl_lead as t1')
            ->join('tbl_country as t2', 't1.lead_country = t2.country_id', 'INNER')
            ->get()
            ->result_array();
    }

    public function get_lead_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('tbl_lead');
            return $query->result_array();
        }

        return $this->db->select('t1.*, t2.country_name')
            ->from('tbl_lead as t1')
            ->where('t1.lead_id', $id)
            ->join('tbl_country as t2', 't1.lead_country = t2.country_id', 'INNER')
            ->get()
            ->row_array();
    }

    public function set_lead()
    {
        $data = array(
            'lead_id' => $this->lead_id,
            'lead_name' => $this->lead_name,
            'lead_email' => $this->lead_email,
            'lead_phone' => $this->lead_phone,
            'lead_country' => $this->lead_country,
            'lead_campaign' => $this->lead_campaign,
            'lead_source' => $this->lead_source,
            'lead_medium' => $this->lead_medium,
        );

        if ($this->lead_id == null) {
            return $this->db->insert('tbl_lead', $data);
        } else {
            $this->db->where('lead_id', $this->lead_id);
            return $this->db->update('tbl_lead', $data);
        }
    }

    public function delete_lead()
    {
        $this->db->where('lead_id', $this->lead_id);
        return $this->db->delete('tbl_lead');
    }
}