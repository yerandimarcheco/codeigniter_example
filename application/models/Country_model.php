<?php
/**
 * Description: Country Model. This is the model to the country.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */

class Country_model extends CI_Model
{
    private static $db;

    public function __construct() {
        parent::__construct();
        self::$db = &get_instance()->db;
    }

    public static function get_countries()
    {
        $query = self::$db->get('tbl_country');
        return $query->result_array();
    }

}