<?php
/**
 * Description: Lead Controller. This is the controller to the lead.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */

defined('BASEPATH') OR exit('No direct script access allowed');

class Lead extends CI_Controller
{
    private $model;

    /**
     * Lead constructor.
     */
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('lead_model');
        $this->load->model('country_model');
        $this->model = new Lead_model();
    }

    /**
     * Function index, function to list all leads
     */
    public function index()
    {
        $data = array();
        $data['leads'] = $this->model->get_leads();
        $this->load->view('templates/header');
        $this->load->view("lead/index", $data);
        $this->load->view('templates/footer');
    }

    /**
     * Function to create a new lead
    */
    public function create()
    {
        if ($this->model->load($this->input->post())) {
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email', 'Email', 'required|is_unique[tbl_lead.lead_email]');

            if ($this->form_validation->run() == FALSE) {
                $data = $this->renderCreateEditView('create', (array)$this->model);
                $this->load->view('templates/header');
                $this->load->view("lead/create", $data);
                $this->load->view('templates/footer');
                return;
            } else {
                if ($this->model->set_lead()) {
                    redirect( base_url() . 'index.php/lead');
                } else {
                    show_error("Unexpected error");
                }
            }
        }
        $data = $this->renderCreateEditView('create');
        $this->load->view('templates/header');
        $this->load->view("lead/create", $data);
        $this->load->view('templates/footer');
    }

    /**
     * Function to delete one lead
    */
    public function delete()
    {
        $id = $this->uri->segment(4);

        if (empty($id))
        {
            show_404();
        }

        $leadItem = $this->model->get_lead_by_id($id);
        $this->model->lead_id = $leadItem['lead_id'];
        if ($this->model->delete_lead()) {
            redirect( base_url() . 'index.php/lead');
        } else {
            show_error("Unexpected error");
        }
    }

    /**
     * Function to view one lead detail
    */
    public function view($lead = null)
    {
        $data['lead'] = $this->model->get_lead_by_id($lead);

        if (empty($data['lead']))
        {
            show_404();
        }

        $this->load->view('templates/header');
        $this->load->view('lead/view', $data);
        $this->load->view('templates/footer');
    }

    /**
     * Function to edit the lead data
    */
    public function edit($lead = null)
    {
        $model = $this->model->get_lead_by_id($lead);

        if ($this->model->load($this->input->post()) && $this->model->set_lead()) {
            redirect( base_url() . 'index.php/lead');
        }

        $data = $this->renderCreateEditView('edit', $model);

        $this->load->view('templates/header');
        $this->load->view("lead/edit", $data);
        $this->load->view('templates/footer');
    }

    /**
     * Function to prepare the data to render in the create/edit view
    */
    private function renderCreateEditView($action, $model = null)
    {
        $data = array();
        $countries = Country_model::get_countries();
        $data['data']['action'] = $action;
        $data['data']['model'] = $model;
        $data['data']['countries'] = $countries;

        return $data;
    }
}