<?php
/**
 * Description: Index view. This view is used to list the leads.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */
?>
<?php $this->load->helper('url'); ?>
<?php $this->load->library('form_validation'); ?>
<div class="container">
    <div class="wrap">
        <h1>Leads</h1>

        <p><a class="btn btn-success" href="lead/create">Create Lead</a></p>

        <div id="w0" class="grid-view">
            <table class="table table-striped table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Lead Name</th>
                    <th>Lead Email</th>
                    <th>Lead Phone</th>
                    <th>Lead Country</th>
                    <th>Lead Campaign</th>
                    <th>Lead Source</th>
                    <th>Lead Medium</th>
                    <th class="action-column">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php $count = 1;?>
                <?php foreach ($leads as $lead): ?>
                    <tr data-key="2">
                        <td><?= $count;?></td>
                        <td><?= $lead['lead_name'];?></td>
                        <td><a href="<?= $lead['lead_email'];?>"><?= $lead['lead_email'];?></a></td>
                        <td><?= $lead['lead_phone'];?></td>
                        <td><?= $lead['country_name'];?></td>
                        <td><?= $lead['lead_campaign'];?></td>
                        <td><?= $lead['lead_source'];?></td>
                        <td><?= $lead['lead_medium'];?></td>
                        <td>
                            <a href="lead/view/<?= $lead['lead_id']?>" title="View" aria-label="View"
                               data-pjax="0"><span class="glyphicon glyphicon-eye-open"></span>
                            </a>
                            <a href="lead/edit/<?= $lead['lead_id']?>" title="Update" aria-label="Update"
                                    data-pjax="0"><span class="glyphicon glyphicon-pencil"></span>
                            </a>
                            <a href="lead/delete/id/<?= $lead['lead_id']?>" title="Delete" aria-label="Delete"
                                    data-pjax="0" data-confirm="Are you sure you want to delete this item?"
                                    data-method="post"><span class="glyphicon glyphicon-trash"></span>
                            </a>
                        </td>
                    </tr>
                    <?php $count++;?>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</div>
