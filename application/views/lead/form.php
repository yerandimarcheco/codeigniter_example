<?php
/**
 * Description: Form view. This view is used to create/update one lead.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */
?>
<?php $this->load->library('form_validation');?>
<?php echo validation_errors(); ?>
<form method="post"  id="leadForm" class="form-horizontal col-md-6 col-md-offset-3"  action="<?php echo site_url('index.php/lead/'.$data['action']); ?>">
    <input type="hidden" value="<?= ($data['model'] !== null && isset($data['model']['lead_id'])) ? $data['model']['lead_id'] : ''?>" name="lead_id">
    <div class="form-group">
        <label for="input1" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" value="<?= ($data['model'] !== null) ? $data['model']['lead_name'] : ''?>" name="name"  class="form-control" id="input1" placeholder="Name"/>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <div class="form-group">
        <label for="input2" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" name="email" value="<?= ($data['model'] !== null) ? $data['model']['lead_email'] : ''?>" class="form-control" id="input2" placeholder="Email"/>
            <div class="help-block with-errors"></div>
        </div>
    </div>

    <div class="form-group">
        <label for="input3" class="col-sm-2 control-label">Phone</label>
        <div class="col-sm-10">
            <input type="phone" name="phone" value="<?= ($data['model'] !== null) ? $data['model']['lead_phone'] : ''?>" class="form-control" id="input3" placeholder="Phone" />
        </div>
    </div>

    <div class="form-group">
        <label for="input4" class="col-sm-2 control-label">Country</label>
        <div class="col-sm-10">
            <select name="country" class="form-control" id="input4">
                <option value="">Select Your Country</option>
                <?php foreach ($data['countries'] as $country):?>
                    <option <?= ($data['model'] !== null && $data['model']['lead_country'] === $country['country_id']) ? 'selected' : '' ?> value="<?= $country['country_id'];?>"><?= $country['country_name'];?></option>
                <?php endforeach;?>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input5" class="col-sm-2 control-label">Campaign</label>
        <div class="col-sm-10">
            <input type="text" name="campaign" value="<?= ($data['model'] !== null) ? $data['model']['lead_campaign'] : ''?>" class="form-control" id="input5" placeholder="Campaign" />
        </div>
    </div>

    <div class="form-group">
        <label for="input6" class="col-sm-2 control-label">Source</label>
        <div class="col-sm-10">
            <select name="source" class="form-control" id="input6">
                <option value="">Select Your Source</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_source'] === 'Facebook') ? 'selected' : ''?> value="Facebook">Facebook</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_source'] === 'Twitter') ? 'selected' : ''?> value="Twitter">Twitter</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_source'] === 'Web') ? 'selected' : ''?> value="Web">Web</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_source'] === 'Mailing') ? 'selected' : ''?> value="Mailing">Mailing</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_source'] === 'Other') ? 'selected' : ''?> value="Other">Other</option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label for="input7" class="col-sm-2 control-label">Medium</label>
        <div class="col-sm-10">
            <select name="medium" class="form-control" id="input7">
                <option value="">Select Your Medium</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_medium'] === 'Article') ? 'selected' : ''?> value="Article">Article</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_medium'] === 'Newsletter') ? 'selected' : ''?> value="Newsletter">Newsletter</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_medium'] === 'Banner') ? 'selected' : ''?> value="Banner">Banner</option>
                <option <?= ($data['model'] !== null && $data['model']['lead_medium'] === 'Other') ? 'selected' : ''?> value="Other">Other</option>
            </select>
        </div>
    </div>

    <input type="submit" class="btn btn-primary col-md-2 col-md-offset-10" value="submit" />
</form>

<script type="text/javascript">
    $(document).ready(function() {
        $('#leadForm').bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                name: {
                    message: 'The name is not valid',
                    validators: {
                        notEmpty: {
                            message: 'The name is required and can\'t be empty'
                        },
                        stringLength: {
                            min: 3,
                            max: 30,
                            message: 'The name must be more than 3 and less than 30 characters long'
                        },
                        regexp: {
                            regexp: /^[a-z ,.'-]+$/i,
                            message: 'The name can only consist of alphabetical, number, dot and underscore'
                        }
                    }
                },
                country: {
                    validators: {
                        notEmpty: {
                            message: 'The country is required and can\'t be empty'
                        }
                    }
                },
                email: {
                    validators: {
                        notEmpty: {
                            message: 'The email address is required and can\'t be empty'
                        },
                        emailAddress: {
                            message: 'The input is not a valid email address'
                        }
                    }
                },
                source: {
                    validators: {
                        notEmpty: {
                            message: 'The source is required and can\'t be empty'
                        }
                    }
                },
                medium: {
                    validators: {
                        notEmpty: {
                            message: 'The medium is required and can\'t be empty'
                        }
                    }
                }
            }
        });
    });
</script>
</body>
</html>
