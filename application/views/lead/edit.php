<?php
/**
 * Description: Create view. This view is used to create a new lead.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */
?>
<?php
$this->load->helper('url');
$this->title = 'Application example - Update Lead ' . $data['model']['lead_name'];
?>

<!DOCTYPE html>
<html>
<head>
    <title>Application example - Create Lead</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css" >

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.css" >

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/styles.css" >
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrapValidator.css"/>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/images/favicon.ico"/>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrapValidator.js"></script>
</head>
<body>
<div class="container">
    <div class="row">
        <h2><?= $this->title;?></h2>
    </div>
    <div class="row">
        <?php $this->load->view('lead/form', $data);?>
    </div>
</div>
</body>
</html>
