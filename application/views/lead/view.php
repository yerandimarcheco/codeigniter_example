<?php
/**
 * Description: Detail view. This view is used to list the lead detail.
 * Author: Yerandi Marcheco Diaz
 * Email: yeran.marcello@gmail.com
 * Initial version created on: 04/02/19
 */
?>
<?php $this->load->helper('url'); ?>
<?php $this->load->library('form_validation'); ?>
<!DOCTYPE html>
<html>
<head>
    <title>Application example - Leads</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrapValidator.css"/>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/images/favicon.ico"/>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="lead-view">

        <h1><?= $lead['lead_name']; ?></h1>

        <p>
            <a class="btn btn-primary" href="<?php echo site_url('index.php/lead/edit/'.$lead['lead_id']); ?>">Update</a>
            <a
                    class="btn btn-danger" href="<?php echo site_url('index.php/lead/delete/id/'.$lead['lead_id']); ?>"
                    data-confirm="Are you sure you want to delete this item?" data-method="post">Delete
            </a>
        </p>

        <table id="w0" class="table table-striped table-bordered detail-view">
            <tr>
                <th>Lead ID</th>
                <td><?= $lead['lead_id']; ?></td>
            </tr>
            <tr>
                <th>Lead Name</th>
                <td><?= $lead['lead_name']; ?></td>
            </tr>
            <tr>
                <th>Lead Email</th>
                <td><a href="mailto:<?= $lead['lead_email']; ?>"><?= $lead['lead_email']; ?></a></td>
            </tr>
            <tr>
                <th>Lead Phone</th>
                <td><?= $lead['lead_phone']; ?></td>
            </tr>
            <tr>
                <th>Lead Country</th>
                <td><?= $lead['country_name']; ?></td>
            </tr>
            <tr>
                <th>Lead Campaign</th>
                <td><?= $lead['lead_campaign']; ?></td>
            </tr>
            <tr>
                <th>Lead Source</th>
                <td><?= $lead['lead_source']; ?></td>
            </tr>
            <tr>
                <th>Lead Medium</th>
                <td><?= $lead['lead_medium']; ?></td>
            </tr>
        </table>
    </div>
</div>
</body>
</html>
