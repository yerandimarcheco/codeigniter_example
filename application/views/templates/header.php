<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->helper('url');
$this->title = 'Application example - Create Lead';
?>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="utf-8">
    <title>Application example</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.css">

    <link rel="stylesheet" href="<?php echo base_url(); ?>css/styles.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/bootstrapValidator.css"/>
    <link rel="shortcut icon" type="image/png" href="<?php echo base_url(); ?>/images/favicon.ico"/>

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>js/bootstrapValidator.js"></script>
</head>
<body>

<div class="wrap">
    <nav id="w0" class="navbar-inverse navbar-fixed-top navbar">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#w0-collapse"><span
                            class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span></button>
                <a class="navbar-brand" href="<?php echo site_url('index.php'); ?>">Application example</a></div>
            <div id="w0-collapse" class="collapse navbar-collapse">
                <ul id="w1" class="navbar-nav navbar-right nav">
                    <li class="active"><a href="<?php echo site_url('index.php'); ?>">Home</a></li>
                    <li><a href="<?php echo site_url('index.php/lead'); ?>">Leads</a></li>
                </ul>
    </nav>

